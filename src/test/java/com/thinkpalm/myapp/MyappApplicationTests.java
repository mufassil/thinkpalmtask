package com.thinkpalm.myapp;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.thinkpalm.myapp.controller.JwtAuthenticationController;
import com.thinkpalm.myapp.controller.UserController;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MyappApplicationTests {
	
	@Autowired
	UserController userController;
	
	@Autowired
	JwtAuthenticationController jwtAuthenticationController;

	@Test
	void contextLoads() {
		Assertions.assertThat(userController).isNotNull();
		Assertions.assertThat(jwtAuthenticationController).isNotNull();
	}

}
