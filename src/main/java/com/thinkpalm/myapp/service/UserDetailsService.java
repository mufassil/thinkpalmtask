/**
 * 
 */
package com.thinkpalm.myapp.service;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.thinkpalm.myapp.model.UserDetails;

/**
 * @author mufassil
 *
 */
@Component
public interface UserDetailsService {

	public Page<UserDetails> findAll(int page, int size, String sortField, String sortDirection) throws Exception;

	public int deleteUser(Integer id);

	Map<String, Object> findAllPaginated(int page, int size, String sortField, String sortDirection) throws Exception;
}
