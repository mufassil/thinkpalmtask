/**
 * 
 */
package com.thinkpalm.myapp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.thinkpalm.myapp.dao.UserDetailsRepo;
import com.thinkpalm.myapp.model.UserDetails;

/**
 * @author mufassil
 *
 */

@Service
@CacheConfig(cacheNames = { "users" }) // store in 'users' cache key
public class UserDetailsServiceImpl implements UserDetailsService {

	private Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	private static final String ID_MUST_NOT_BE_NULL = "The given id must not be null!";

	@Autowired
	UserDetailsRepo userRepo;

	private Sort.Direction getSortDirection(String direction) {
		if (direction.equals("asc")) {
			return Sort.Direction.ASC;
		} else if (direction.equals("desc")) {
			return Sort.Direction.DESC;
		}

		return Sort.Direction.ASC;
	}

	@Override
	@Cacheable(value = "users", key = "#page") // caches the result of the method
	public Page<UserDetails> findAll(int page, int size, String sortField, String sortDirection) throws Exception {
		logger.info("Filters to get User Details");
		logger.info("Page: " + page);
		logger.info("Size: " + size);
		logger.info("Sort Field: " + sortField);
		logger.info("Sort Direction: " + sortDirection);
		try {
			List<Order> orders = new ArrayList<Order>();
			orders.add(new Order(getSortDirection(sortDirection), sortField));
			Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

			return userRepo.findAll(pagingSort);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception("Something went wrong!");
		}
	}

	@Override
	public Map<String, Object> findAllPaginated(int page, int size, String sortField, String sortDirection)
			throws Exception {
		logger.info("Filters to get User Details");
		logger.info("Page: " + page);
		logger.info("Size: " + size);
		logger.info("Sort Field: " + sortField);
		logger.info("Sort Direction: " + sortDirection);
		try {
			Map<String, Object> response = new HashMap<>();
			// page and size should be non-negative number
			if (page >= 0 && size >= 0) {
				// Get All records from Database
				List<UserDetails> userDetails = userRepo.findAll();

				// Get records with del_flag as false
				userDetails = userDetails.stream().filter(p -> p.getDel_flag() == false)
						.collect(Collectors.toList());
				
				int skipCount = page * size;
				List<UserDetails> pageResult = userDetails.stream().skip(skipCount).limit(size)
						.collect(Collectors.toList());
				
				

				long totalPages = userDetails.size() / size + 1;
				response.put("users", pageResult);
				response.put("currentPage", page);
				response.put("totalItems", userDetails.size());
				response.put("totalPages", totalPages);
				response.put("currentRecords", pageResult.size());
			} else {
				response.put("page and size parameter should be greater than zero", null);
			}

			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception("Something went wrong!");
		}
	}

	@Override
	@Transactional
	@CachePut(value = "user", key = "#id")
	public int deleteUser(Integer id) {
		try {
			Assert.notNull(id, ID_MUST_NOT_BE_NULL);
			return userRepo.updateUser(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw e;
		}
	}
}
