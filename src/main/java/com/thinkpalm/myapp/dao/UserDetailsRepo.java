/**
 * 
 */
package com.thinkpalm.myapp.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thinkpalm.myapp.model.UserDetails;

/**
 * @author mufassil
 *
 */
@Repository
public interface UserDetailsRepo
		extends PagingAndSortingRepository<UserDetails, Integer>, JpaSpecificationExecutor<UserDetails> {

	@Transactional
	//@Query("from UserDetails ud  where ud.del_flag =0")
	public List<UserDetails> findAll();

	@Transactional
	@Modifying
	@Query("update UserDetails set del_flag = 1 where id = :id")
	public int updateUser(@Param("id") Integer id);

}
