package com.thinkpalm.myapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thinkpalm.myapp.model.UserDetails;
import com.thinkpalm.myapp.service.UserDetailsService;

@RestController
@CrossOrigin
public class UserController {

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserDetailsService userService;

	@DeleteMapping("/user/{uid}")
	public ResponseEntity<String> deleteUser(@PathVariable("uid") Integer uid) {
		try {
			logger.info("Delete user with id: " + uid);
			int flag = userService.deleteUser(uid);
			if (flag > 0) {
				return ResponseEntity.status(HttpStatus.OK).body("User deleted successfully!");
			} else {
				return ResponseEntity.status(HttpStatus.OK).body("User is not available.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong!");
		}
	}

	@GetMapping("/users")
	public ResponseEntity<Map<String, Object>> getAllUsers(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size, @RequestParam(defaultValue = "id") String sortField,
			@RequestParam(defaultValue = "desc") String sortDirection) {

		try {
			logger.info("Getting User details... ");
			Map<String, Object> response = new HashMap<>();
			// page and size should be non-negative number
			if (page >= 0 && size >= 0) {
				List<UserDetails> userDetails = new ArrayList<UserDetails>();

				Page<UserDetails> pageTuts = userService.findAll(page, size, sortField, sortDirection);
				userDetails = pageTuts.getContent();

				response.put("users", userDetails);
				response.put("currentPage", pageTuts.getNumber());
				response.put("totalItems", pageTuts.getTotalElements());
				response.put("totalPages", pageTuts.getTotalPages());
			} else {
				response.put("page and size parameter should be greater than zero", null);
			}

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/page/users")
	public ResponseEntity<Map<String, Object>> getAllUsersPaginated(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size, @RequestParam(defaultValue = "id") String sortField,
			@RequestParam(defaultValue = "desc") String sortDirection) {

		try {
			logger.info("Getting User details... ");

			Map<String, Object> response = userService.findAllPaginated(page, size, sortField, sortDirection);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// @ExceptionHandler annotation tells it to catch any instance of
	// RuntimeException within the endpoint functions and return a 500 response
	@ExceptionHandler(RuntimeException.class)
	public final ResponseEntity<Exception> handleAllExceptions(RuntimeException ex) {
		return new ResponseEntity<Exception>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
